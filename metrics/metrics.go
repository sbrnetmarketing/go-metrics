package metrics

import (
	"fmt"

	"time"

	"github.com/cactus/go-statsd-client/statsd"
)

const defaultSampleRate = 1.0
const defaultPrecision = time.Millisecond

// TimerInterface is the set of methods a conforming timer implementation must provide
type TimerInterface interface {
  SetPrecision(precision time.Duration)
  Report(metricKey string)
}

// MetricInterface is the set of methods a conforming metric implementation must provide
type MetricInterface interface {
  NewTimer() TimerInterface
  SetPrecision(precision time.Duration)
  AddCounter(metricKey string, value int64)
  AddGauge(metricKey string, value int64)
  AddTiming(metricKey string, value int64)
}

// Metric contains the configuration and the client for sending metrics
type Metric struct {
	client      statsd.Statter
	application string
	host        string
	port        int
	sampleRate  float32
	precision   time.Duration
}

//Timer holds the start time and allows for timing processes in the application
type Timer struct {
	startTime time.Time
	m         *Metric
	precision time.Duration
}

//AppMetric is a shared Metric instance
var AppMetric MetricInterface

//NewMetric returns a pointer to a Metric instance
func NewMetric(application string, host string, port int) (m MetricInterface, err error) {
	address := fmt.Sprintf("%s:%d", host, port)
	c, err := statsd.NewClient(address, application)
	if err == nil {
		m = &Metric{
			client:      c,
			application: application,
			host:        host,
			port:        port,
			sampleRate:  defaultSampleRate,
			precision:   defaultPrecision}
	}
	return m, err
}

//NewTimer starts and returns a pointer to a new Timer
func (m *Metric) NewTimer() (t TimerInterface) {
	t = &Timer{
		startTime: time.Now(),
		m:         m,
		precision: m.precision}
	return t
}

//SetPrecision sets the precision for NewTimers
func (m *Metric) SetPrecision(precision time.Duration) {
	m.precision = precision
}

//SetPrecision sets the precision for an existing timer
func (t *Timer) SetPrecision(precision time.Duration) {
	t.precision = precision
}

//AddCounter sends a counter metric
func (m *Metric) AddCounter(metricKey string, value int64) {
	go m.client.Inc(metricKey, value, m.sampleRate)
}

//AddGauge sends a gauge metric
func (m *Metric) AddGauge(metricKey string, value int64) {
	go m.client.Gauge(metricKey, value, m.sampleRate)
}

//AddTiming sends a timer metric in ms
func (m *Metric) AddTiming(metricKey string, value int64) {
	go m.client.Timing(metricKey, value, m.sampleRate)
}

//Report stops an active timer and sends the timing metric
func (t *Timer) Report(metricKey string) {
	stopTime := time.Now()
	diff := stopTime.Sub(t.startTime)
	t.m.AddTiming(metricKey, int64(diff/t.precision))
}
