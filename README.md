# SBR Go-metrics

## Introduction

Go library for writing metrics.

## Installation

go get bitbucket.org/sbrnetmarketing/go-metrics/metrics

## Sample Usage

```go
package main

import (
	"fmt"
	"time"

	"bitbucket.org/sbrnetmarketing/go-metrics/metrics"
)

func main() {
	var err error
    //metrics.AppMetric is globally accessable anywhere you import the library in your application
	metrics.AppMetric, err = metrics.NewMetric("test-service", "localhost", 8125)
	if err != nil {
		panic(fmt.Sprintf("CAN'T LOAD METRICS! %s", err))
	}

	//Set the default preceision to nano seconds for all Timers
    metrics.AppMetric.SetPrecision(time.Nanosecond)

	//Start a timer
    timer1 := metrics.AppMetric.NewTimer()

	metrics.AppMetric.AddCounter("test.counter.metric", 123)

	//Create a second timer with a different precision
    timer2 := metrics.AppMetric.NewTimer()
	timer2.SetPrecision(time.Microsecond)

	//Add a manual timed value
    metrics.AppMetric.AddTiming("test.timer.metric", 123)

	//Stop the timer and report the metric
    timer2.Report("test.in_process_timer.metric2")

	metrics.AppMetric.AddGauge("test.gauge.metric", 123)

	timer1.Report("test.in_process_timer.metric")

    //Also can create an instance locally
    localMetricVar, _ := metrics.NewMetric("other-service", "otherhost", 8125)
    localMetricVar.AddCounter("some.other.metric", 999)

    otherFunc()
}

func otherFunc() {
    metrics.AppMetric.AddCounter("other_context.metric", 456)
}

```